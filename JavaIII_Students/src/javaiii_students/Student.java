/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaiii_students;

import java.util.Comparator;

public class Student {

    public Student(int id, String name, byte[] image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    int id;
    String name;
    byte[] image; //blob

    @Override
    public String toString() {
        return String.format("%d: %s", id, name);
    }
    public static final Comparator<Student> compareByName = new Comparator<Student>(){
        @Override
        public int compare(Student o1, Student o2){
            return o1.name.compareTo(o2.name);
        }
    };
 
    /*   public static final Comparator<Student> compareByID = new Comparator<Student>(){
        @Override
        public int compare(Student o1, Student o2){
            return o1.id.compareTo(o2.id);
        }
    };
    public static final Comparator<Student> compareById = (Student o1, Student o2) -> o1
    */        

    class StudentByNameComparator implements Comparator<Student>{
    @Override
    public int compare(Student o1, Student o2){
        return o1.name.compareTo(o2.name);
    }
}
}
