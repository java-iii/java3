/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaiii_students;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import java.util.ArrayList;


public class Database {
     private static final String dbURL = "jdbc:mysql://localhost:3306/day10students";
        private static final String username = "root";
        private static final String password = "root2021";
        private Connection conn;
        
        public Database() throws SQLException{
        
            conn = DriverManager.getConnection(dbURL, username, password);
        }
    public ArrayList<Student> getAllStudents() throws SQLException{
        ArrayList<Student> list = new ArrayList<>();
        String sql="SELECT * FROM students";
        PreparedStatement statement= conn.prepareStatement(sql);
        try(ResultSet result = statement.executeQuery(sql)){
            while(result.next()){
                int id = result.getInt("id");
                String name = result.getString("name");
                
               
                list.add(new Student(id,name,null));
            }
        }
        return list;
    }
   public void addStudent(Student student) throws SQLException {
        String sql = "INSERT INTO students VALUES (NULL, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, student.name);
        statement.setBytes(2, student.image);
        //TODO: handle image
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record Inserted");
    }
   
    Student getStudentById(int id) throws SQLException {
        PreparedStatement stmtSelect = conn.prepareStatement("SELECT * FROM students WHERE id=?");
        stmtSelect.setInt(1, id);
        try (ResultSet resultSet = stmtSelect.executeQuery()) {
            if (resultSet.next()) {
                String name = resultSet.getString("name");
                byte[] image = resultSet.getBytes("image");
                return new Student(id, name, image);
            } else {
                throw new SQLException("Record not found");
            }
        }
    }
     public void removeStudent(Student student)throws SQLException{
            String sql="DELETE FROM students WHERE id=?";
            PreparedStatement statement= conn.prepareStatement(sql);
            statement.setInt(1,student.id);
            statement.executeUpdate();
            System.out.println("Record updated "+ student.name+" Removed");
            
        }
     public void removeImage(Student student)throws SQLException{
         String sql="UPDATE students SET image =? WHERE id=?";
         PreparedStatement statement= conn.prepareStatement(sql);
         statement.setBytes(1, null);
         statement.setInt(2,student.id);
         statement.executeUpdate();
         
     }  
   public void updateStudent(Student student)throws SQLException{
         String sql="UPDATE students SET name=? image =? WHERE id=?";
         PreparedStatement statement= conn.prepareStatement(sql);
         statement.setString(1,student.name);
         statement.setBytes(2,student.image);
         statement.setInt(3,student.id);
         statement.executeUpdate();
         
     }  
     
}
