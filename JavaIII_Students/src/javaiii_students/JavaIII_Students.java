package javaiii_students;

import com.opencsv.CSVWriter;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.List;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author nxeno
 */
public class JavaIII_Students extends javax.swing.JFrame {

    DefaultListModel<Student> studentListModel = new DefaultListModel<>();
    String sortColumn = "id";
    Database db;
    Student currentEditedStudent = null;
    public JavaIII_Students() {

        try {
            initComponents();
            fileChooserImage.setFileFilter(new FileNameExtensionFilter("Images (*.gif,*.png,*.jpg,*.jpeg)", "gif", "png", "jpg", "jpeg"));
            jFileChooserCsv.setFileFilter(new FileNameExtensionFilter("CSV files (*.csv)", "csv"));
            db = new Database();
            reloadFromDatabase();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to Connect" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);//FATAL ERROR
        }
    }

    private void reloadFromDatabase() {
        try {
           // ArrayList<Student> studentList = 
            ArrayList<Student> list = db.getAllStudents();
            switch(sortColumn){
                case"id":
                    Collections.sort(list,(Student o1, Student o2)-> o1.id -o2.id);
                    break;
                case"name":
                    Collections.sort(list,Student.compareByName);
                    break;
                default://should never happen
                 JOptionPane.showMessageDialog(this,
                         "INVALID CONTROL FLOW (122322)",
                         "Internal ERROR",
                         JOptionPane.ERROR_MESSAGE);
            }
            studentListModel.clear();
            for (Student student : list) {
                studentListModel.addElement(student);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to Connect" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            
        }
    }
    public void reload(){
       
    }
    public void showPopupMenu(MouseEvent evt) {
        popupMenu.show(this, evt.getX(), evt.getY());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgAddEdit = new javax.swing.JDialog();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        dlgAddEdit_lblID = new javax.swing.JLabel();
        dlgAddEdit_tfName = new javax.swing.JTextField();
        dlgAddEdit_lblImage = new javax.swing.JLabel();
        dlgAddEdit_btCancel = new javax.swing.JButton();
        dlgAddEdit_btSave = new javax.swing.JButton();
        dlgAddEdit_btRemovePicture = new javax.swing.JButton();
        fileChooserImage = new javax.swing.JFileChooser();
        jFileChooserCsv = new javax.swing.JFileChooser();
        popupMenu = new javax.swing.JPopupMenu();
        miPopupEdit = new javax.swing.JMenuItem();
        miPopupDelete = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstStudentList = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miExport = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        miFileExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        miEditAddStudent = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        miSortByID = new javax.swing.JMenuItem();
        miSortByName = new javax.swing.JMenuItem();

        jLabel2.setText("ID:");

        jLabel3.setText("Name:");

        jLabel4.setText("Photo(click to pick)");

        dlgAddEdit_lblID.setText("-");

        dlgAddEdit_tfName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_tfNameActionPerformed(evt);
            }
        });

        dlgAddEdit_lblImage.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        dlgAddEdit_lblImage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dlgAddEdit_lblImageMouseClicked(evt);
            }
        });

        dlgAddEdit_btCancel.setText("Cancel");
        dlgAddEdit_btCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btCancelActionPerformed(evt);
            }
        });

        dlgAddEdit_btSave.setText("Save");
        dlgAddEdit_btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btSaveActionPerformed(evt);
            }
        });

        dlgAddEdit_btRemovePicture.setText("Remove picture");
        dlgAddEdit_btRemovePicture.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btRemovePictureActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dlgAddEditLayout = new javax.swing.GroupLayout(dlgAddEdit.getContentPane());
        dlgAddEdit.getContentPane().setLayout(dlgAddEditLayout);
        dlgAddEditLayout.setHorizontalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddEditLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dlgAddEdit_btRemovePicture))
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(dlgAddEditLayout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel2)))
                            .addGroup(dlgAddEditLayout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(dlgAddEdit_btSave, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 10, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dlgAddEdit_btCancel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(dlgAddEdit_tfName, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                        .addComponent(dlgAddEdit_lblID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(dlgAddEdit_lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(61, 61, 61))
        );
        dlgAddEditLayout.setVerticalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(dlgAddEdit_lblID))
                .addGap(18, 18, 18)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(dlgAddEdit_tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgAddEditLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(34, 34, 34)
                        .addComponent(dlgAddEdit_btRemovePicture))
                    .addComponent(dlgAddEdit_lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgAddEdit_btCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dlgAddEdit_btSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
        );

        miPopupEdit.setText("Edit");
        miPopupEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miPopupEditActionPerformed(evt);
            }
        });
        popupMenu.add(miPopupEdit);

        miPopupDelete.setText("Delete");
        miPopupDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miPopupDeleteActionPerformed(evt);
            }
        });
        popupMenu.add(miPopupDelete);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(350, 200));

        lstStudentList.setModel(studentListModel);
        lstStudentList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstStudentListMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lstStudentListMouseReleased(evt);
            }
        });
        jScrollPane1.setViewportView(lstStudentList);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jLabel1.setText("...");
        getContentPane().add(jLabel1, java.awt.BorderLayout.PAGE_END);

        jMenu1.setText("File");

        miExport.setText("Exprt");
        miExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExportActionPerformed(evt);
            }
        });
        jMenu1.add(miExport);
        jMenu1.add(jSeparator1);

        miFileExit.setText("Exit");
        miFileExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFileExitActionPerformed(evt);
            }
        });
        jMenu1.add(miFileExit);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        miEditAddStudent.setText("Add Student");
        miEditAddStudent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miEditAddStudentActionPerformed(evt);
            }
        });
        jMenu2.add(miEditAddStudent);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Sort");

        miSortByID.setText("Sort by ID");
        miSortByID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSortByIDActionPerformed(evt);
            }
        });
        jMenu3.add(miSortByID);

        miSortByName.setText("Sort by Name");
        miSortByName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miSortByNameActionPerformed(evt);
            }
        });
        jMenu3.add(miSortByName);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void dlgAddEdit_tfNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_tfNameActionPerformed


    }//GEN-LAST:event_dlgAddEdit_tfNameActionPerformed

    public static BufferedImage resize(BufferedImage img, int newW, int newH) {
        // FIXME: keep original width/height ratio
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }

    private BufferedImage byteArrayToBufferedImage(byte[] imageData) throws IOException {
        ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        return ImageIO.read(bais);
    }

    byte[] bufferedImageToByteArray(BufferedImage bi) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (!ImageIO.write(bi, "png", baos)) {
            throw new IOException("Error creating png data");
        }
        byte[] imageBytes = baos.toByteArray();
        return imageBytes;
    }

    BufferedImage currentBuffImage;


    private void dlgAddEdit_lblImageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dlgAddEdit_lblImageMouseClicked
        if (fileChooserImage.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {

                File chosenFile = fileChooserImage.getSelectedFile();

                currentBuffImage = ImageIO.read(chosenFile);
                // FIXME: use constants or ask JLabel
                currentBuffImage = resize(currentBuffImage, dlgAddEdit_lblImage.getSize().width, dlgAddEdit_lblImage.getSize().height);
                Icon icon = new ImageIcon(currentBuffImage);

                dlgAddEdit_lblImage.setIcon(icon);

            } catch (IOException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        "File error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_dlgAddEdit_lblImageMouseClicked

    private void dlgAddEdit_btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btSaveActionPerformed
         try {
            // check for errors and collect them
            ArrayList<String> errorList = new ArrayList<>();
            String name = dlgAddEdit_tfName.getText();
            if (name.length() < 2 || name.length() > 45) {
                errorList.add("Name must be 2-45 characters long");
            }
            //
            if (!errorList.isEmpty()) {
                JOptionPane.showMessageDialog(this, String.join("\n", errorList),
                        "Input error(s)", JOptionPane.ERROR_MESSAGE);
                return;
            }
            // prepare bytes of image for insert
            byte[] imageBytes = null;
            // convert to current image to byte array
            if (currentBuffImage != null) {
                imageBytes = bufferedImageToByteArray(currentBuffImage); // IOException
            }
            if (currentEditedStudent == null) { // add
                Student student = new Student(0, name, imageBytes);
                db.addStudent(student);
            } else { 
                
                currentEditedStudent.name = name;
                currentEditedStudent.image = imageBytes;
                db.updateStudent(currentEditedStudent);
                
                
            }
            reloadFromDatabase();
            dlgAddEdit.setVisible(false); // dismiss the dialog
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Database error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Data error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_dlgAddEdit_btSaveActionPerformed

    private void dlgAddEdit_btCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btCancelActionPerformed
        dlgAddEdit.setVisible(false);
    }//GEN-LAST:event_dlgAddEdit_btCancelActionPerformed

    private void miEditAddStudentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miEditAddStudentActionPerformed
         // clean inputs
        dlgAddEdit_tfName.setText("");
        dlgAddEdit_lblID.setText("");
        dlgAddEdit_lblImage.setIcon(null);
        currentEditedStudent = null;
        currentBuffImage = null;
        // show dialog
        dlgAddEdit.pack();
        dlgAddEdit.setLocationRelativeTo(this);
        dlgAddEdit.setModal(true);
        dlgAddEdit_btSave.setText("Add student");
        dlgAddEdit.setVisible(true);
    }//GEN-LAST:event_miEditAddStudentActionPerformed

    private void lstStudentListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstStudentListMouseClicked
       if (evt.getClickCount() == 2) { // double-click
            int index = lstStudentList.locationToIndex(evt.getPoint());
            lstStudentList.setSelectedIndex(index);
        } else {
            return; // do nothing if it's not a double-click
        }
        Student studentSelected = lstStudentList.getSelectedValue();
        if (studentSelected == null) {
            return; // do nothing, no selection
        }
        try {
            // fetch full record, including the blob / image
            currentEditedStudent = db.getStudentById(studentSelected.id);
            dlgAddEdit_lblID.setText(currentEditedStudent.id + "");
            dlgAddEdit_tfName.setText(currentEditedStudent.name);
            // byte[] array to BufferedImage to Icon
            
            
            if (currentEditedStudent.image == null) {
                dlgAddEdit_lblImage.setIcon(null);
                currentBuffImage = null;
            } else {
                currentBuffImage = byteArrayToBufferedImage(currentEditedStudent.image);
                // FIXME: use constants or ask JLabel
                currentBuffImage = resize(currentBuffImage, dlgAddEdit_lblImage.getSize().width+1, dlgAddEdit_lblImage.getSize().height+1);
                Icon icon = new ImageIcon(currentBuffImage);
                dlgAddEdit_lblImage.setIcon(icon);
            }
            // show dialog
            dlgAddEdit.pack();
            dlgAddEdit.setLocationRelativeTo(this);
            dlgAddEdit.setModal(true);
            dlgAddEdit_btSave.setText("Update student");
            dlgAddEdit.setVisible(true);
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to fetch records: " + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Error loading image: " + ex.getMessage(),
                    "Data error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_lstStudentListMouseClicked

    private void miSortByIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSortByIDActionPerformed
        sortColumn = "id";
        reloadFromDatabase();// TODO add your handling code here:
    }//GEN-LAST:event_miSortByIDActionPerformed

    private void miSortByNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miSortByNameActionPerformed
         sortColumn = "name";
        reloadFromDatabase();
    }//GEN-LAST:event_miSortByNameActionPerformed

    private void miExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExportActionPerformed
         java.util.List<Student> list = lstStudentList.getSelectedValuesList();
        if (list.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Select some records first to export them",
                    "Data export", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        if (jFileChooserCsv.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File chosenFile = jFileChooserCsv.getSelectedFile();
                String chosenFileStr = chosenFile.getCanonicalPath(); // ex
                // Important: append file extension if none was given
                if (!chosenFileStr.matches(".*\\.[^.]{1,10}")) {
                    chosenFileStr += ".csv";
                    chosenFile = new File(chosenFileStr);
                }
                // perform the export
                try (Writer writer = new FileWriter(chosenFile); CSVWriter csvWriter = new CSVWriter(writer)) {
                    String[] headerRecord = {"Id", "Name"};
                    csvWriter.writeNext(headerRecord);
                    for (Student student : list) { // FIXME: what exception does writeNext throw ??? catch it !
                        csvWriter.writeNext(new String[]{student.id + "", student.name});
                    }
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this,
                        "Error writing file:\n" + ex.getMessage(),
                        "File error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_miExportActionPerformed

    private void miPopupEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miPopupEditActionPerformed
        Student student = lstStudentList.getSelectedValue();
            try {
                dlgAddEdit_tfName.setText(db.getStudentById(student.id).name);
                dlgAddEdit_lblID.setText(String.valueOf(db.getStudentById(student.id).id));
                
                
                if(student.image==null){
                    dlgAddEdit_lblImage.setIcon(null);
                }
                Icon icon = new ImageIcon(byteArrayToBufferedImage(db.getStudentById(student.id).image));
                dlgAddEdit_lblImage.setIcon(icon);
                currentEditedStudent = lstStudentList.getSelectedValue();
            
            } catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this, ex.getMessage(), "Database error",
                        JOptionPane.ERROR_MESSAGE);
            } catch (IOException ex) {
                Logger.getLogger(JavaIII_Students.class.getName()).log(Level.SEVERE, null, ex);
            }
            dlgAddEdit.pack();
            dlgAddEdit.setLocationRelativeTo(this);
            dlgAddEdit.setModal(true);
            dlgAddEdit_btSave.setText("Update student");
            dlgAddEdit.setVisible(true);
    }//GEN-LAST:event_miPopupEditActionPerformed

    private void miPopupDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miPopupDeleteActionPerformed
       try{
           Student student =lstStudentList.getSelectedValue();
           if(student == null){
               return;
           }
            int input = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete? "+student.name , "DELETING...",
            JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null);
            if(input==1||input==2){
                return;
            }
          db.removeStudent(student);
            reloadFromDatabase();
            
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to fetch" + ex.getMessage(),
                    "DB ERROR",
                    JOptionPane.ERROR_MESSAGE);
        }
    

    }//GEN-LAST:event_miPopupDeleteActionPerformed

    private void miFileExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFileExitActionPerformed
    dispose();
    }//GEN-LAST:event_miFileExitActionPerformed

    private void lstStudentListMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstStudentListMouseReleased
     if (evt.isPopupTrigger()) {
            showPopupMenu(evt);
        }
        if (SwingUtilities.isRightMouseButton(evt)) {
            JList list = (JList) evt.getSource();
            int row = list.locationToIndex(evt.getPoint());
            list.setSelectedIndex(row);
        }
    }//GEN-LAST:event_lstStudentListMouseReleased

    private void dlgAddEdit_btRemovePictureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btRemovePictureActionPerformed
      try{
           Student student =lstStudentList.getSelectedValue();
           if(student == null){
               return;
           }
            int input = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete the picture? " , "DELETING...",
            JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null);
            if(input==1){
                return;
            }
        dlgAddEdit_lblImage.setIcon(null);
        
        db.removeImage(student);
        
            
      } catch (SQLException ex) {
           ex.printStackTrace();
            JOptionPane.showMessageDialog(this, ex.getMessage(), "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_dlgAddEdit_btRemovePictureActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JavaIII_Students().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDialog dlgAddEdit;
    private javax.swing.JButton dlgAddEdit_btCancel;
    private javax.swing.JButton dlgAddEdit_btRemovePicture;
    private javax.swing.JButton dlgAddEdit_btSave;
    private javax.swing.JLabel dlgAddEdit_lblID;
    private javax.swing.JLabel dlgAddEdit_lblImage;
    private javax.swing.JTextField dlgAddEdit_tfName;
    private javax.swing.JFileChooser fileChooserImage;
    private javax.swing.JFileChooser jFileChooserCsv;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JList<Student> lstStudentList;
    private javax.swing.JMenuItem miEditAddStudent;
    private javax.swing.JMenuItem miExport;
    private javax.swing.JMenuItem miFileExit;
    private javax.swing.JMenuItem miPopupDelete;
    private javax.swing.JMenuItem miPopupEdit;
    private javax.swing.JMenuItem miSortByID;
    private javax.swing.JMenuItem miSortByName;
    private javax.swing.JPopupMenu popupMenu;
    // End of variables declaration//GEN-END:variables
}
