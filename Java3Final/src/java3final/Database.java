/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java3final;

import java.sql.*;

import java.util.ArrayList;

public class Database {

    private static final String dbURL = "jdbc:mysql://localhost:3306/finaldb";
    private static final String username = "root";
    private static final String password = "root2021";
    private Connection conn;

    public Database() throws SQLException {

        conn = DriverManager.getConnection(dbURL, username, password);
    }

    public ArrayList<Flights> getAllFlights() throws SQLException {
        ArrayList<Flights> list = new ArrayList<>();
        String sql = "SELECT * FROM flights";
        PreparedStatement statement = conn.prepareStatement(sql);
        try ( ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) {
                int id = result.getInt("id");
                Date onDay = result.getDate("onDay");
                String fromCode = result.getString("fromCode");
                String toCode = result.getString("toCode");
                String type = result.getString("type");
                int passengers = result.getInt("passengers");

                list.add(new Flights(id, onDay, fromCode, toCode, type, passengers));
            }
        }
        return list;
    }

    public void addFlight(Flights flight) throws SQLException {
        String sql = "INSERT INTO flights VALUES (NULL,?,?,?,?,?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setDate(1, new java.sql.Date(flight.onDay.getTime()));
        statement.setString(2, flight.fromCode);
        statement.setString(3, flight.toCode);
        statement.setString(4, flight.type);
        statement.setInt(5, flight.passengers);
        //TODO: handle image
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record Inserted");
    }

    Flights getFlightsById(int id) throws SQLException {
        PreparedStatement stmtSelect = conn.prepareStatement("SELECT * FROM flights WHERE id=?");
        stmtSelect.setInt(1, id);
        try ( ResultSet resultSet = stmtSelect.executeQuery()) {
            if (resultSet.next()) {
                //int id = resultSet.getInt("id");
                Date onDay = resultSet.getDate("onDay");
                String fromCode = resultSet.getString("fromCode");
                String toCode = resultSet.getString("toCode");
                String type = resultSet.getString("type");
                int passengers = resultSet.getInt("passengers");

                return new Flights(id, onDay, fromCode, toCode, type, passengers);
            } else {
                throw new SQLException("Record not found");
            }
        }
    }

    public void removeFlight(Flights flight) throws SQLException {
        String sql = "DELETE FROM flights WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, flight.id);
        statement.executeUpdate();
        System.out.println("Record updated " + flight.toString() + " Removed");

    }
}
