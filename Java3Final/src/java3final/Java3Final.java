/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java3final;

import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author nxeno
 */
public class Java3Final extends javax.swing.JFrame {
DefaultListModel<Flights> lstFlightsModel = new DefaultListModel<>();
Database db;    
String sortColumn = "id";
public Java3Final() {
    try{
        initComponents();
        jFileChooserCsv.setFileFilter(new FileNameExtensionFilter("CSV files (*.csv)", "csv"));
        db=new Database();
        reloadFromDatabase();
        Flights currentEditedFlight = null;
    }catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to Connect" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(1);//FATAL ERROR
        }
    
    
    
    }



private void reloadFromDatabase() {
        try {
           
            
           ArrayList<Flights> list = db.getAllFlights();
           
            lstFlightsModel.clear();
            for (Flights flight : list) {
                lstFlightsModel.addElement(flight);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to Connect" + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
            
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        dlgAddFlight = new javax.swing.JDialog();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        dlgTypeCb = new javax.swing.JComboBox<>();
        dlgPassengerSld = new javax.swing.JSlider();
        jLabel7 = new javax.swing.JLabel();
        dlgPasengerLbl = new javax.swing.JLabel();
        dlgIdLbl = new javax.swing.JLabel();
        dlgCancel = new javax.swing.JButton();
        btSave = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        dlgFromTf = new javax.swing.JTextField();
        dlgDateTf = new javax.swing.JTextField();
        dlgToTf = new javax.swing.JTextField();
        jFileChooserCsv = new javax.swing.JFileChooser();
        popUpDelete = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstflights = new javax.swing.JList<>();
        jMenuBar1 = new javax.swing.JMenuBar();
        miFile = new javax.swing.JMenu();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        miFileSave = new javax.swing.JMenuItem();
        miFileExit = new javax.swing.JMenuItem();
        miAdd = new javax.swing.JMenu();

        jList1.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(jList1);

        jLabel2.setText("Flight ID");

        jLabel3.setText("Departure Date");

        jLabel4.setText("From :");

        jLabel5.setText("To:");

        dlgTypeCb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Domestic", "International", "Private" }));

        dlgPassengerSld.setMinimum(1);
        dlgPassengerSld.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                dlgPassengerSldStateChanged(evt);
            }
        });

        jLabel7.setText("Passengers:");

        dlgPasengerLbl.setText("-");

        dlgIdLbl.setText("-");

        dlgCancel.setText("Cancel");
        dlgCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgCancelActionPerformed(evt);
            }
        });

        btSave.setText("Save");
        btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSaveActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        jLabel10.setText("Add A Flight");

        dlgFromTf.setText("enter airport code");

        dlgDateTf.setText("YYYY-MM-DD");

        dlgToTf.setText("enter airport code");

        javax.swing.GroupLayout dlgAddFlightLayout = new javax.swing.GroupLayout(dlgAddFlight.getContentPane());
        dlgAddFlight.getContentPane().setLayout(dlgAddFlightLayout);
        dlgAddFlightLayout.setHorizontalGroup(
            dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddFlightLayout.createSequentialGroup()
                .addGap(0, 118, Short.MAX_VALUE)
                .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgAddFlightLayout.createSequentialGroup()
                        .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(dlgAddFlightLayout.createSequentialGroup()
                                .addComponent(btSave, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(79, 79, 79)
                                .addComponent(dlgCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddFlightLayout.createSequentialGroup()
                                .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(dlgAddFlightLayout.createSequentialGroup()
                                        .addGap(190, 190, 190)
                                        .addComponent(jLabel6))
                                    .addGroup(dlgAddFlightLayout.createSequentialGroup()
                                        .addGap(59, 59, 59)
                                        .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(dlgPassengerSld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddFlightLayout.createSequentialGroup()
                                                .addComponent(jLabel7)
                                                .addGap(47, 47, 47)
                                                .addComponent(dlgPasengerLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(42, 42, 42)))))
                                .addGap(150, 150, 150))
                            .addGroup(dlgAddFlightLayout.createSequentialGroup()
                                .addGap(82, 82, 82)
                                .addComponent(jLabel10)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddFlightLayout.createSequentialGroup()
                        .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(dlgAddFlightLayout.createSequentialGroup()
                                .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2))
                                .addGap(49, 49, 49)
                                .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(dlgIdLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(dlgFromTf, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(dlgDateTf, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(dlgToTf, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(dlgTypeCb, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(105, 105, 105))))
        );
        dlgAddFlightLayout.setVerticalGroup(
            dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddFlightLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(dlgIdLbl))
                .addGap(18, 18, 18)
                .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(dlgDateTf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(dlgFromTf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(dlgToTf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addGap(86, 86, 86)
                .addComponent(dlgTypeCb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(dlgPassengerSld, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(dlgPasengerLbl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(dlgAddFlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btSave, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50))
        );

        jMenuItem1.setText("Delete");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        popUpDelete.add(jMenuItem1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("...");

        lstflights.setModel(lstFlightsModel);
        lstflights.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstflightsMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lstflightsMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(lstflights);

        miFile.setText("File");
        miFile.add(jSeparator1);

        miFileSave.setText("Save");
        miFileSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFileSaveActionPerformed(evt);
            }
        });
        miFile.add(miFileSave);

        miFileExit.setText("Exit");
        miFileExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFileExitActionPerformed(evt);
            }
        });
        miFile.add(miFileExit);

        jMenuBar1.add(miFile);

        miAdd.setText("Add");
        miAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                miAddMouseClicked(evt);
            }
        });
        miAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miAddActionPerformed(evt);
            }
        });
        jMenuBar1.add(miAdd);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 821, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 576, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void miFileSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFileSaveActionPerformed
     java.util.List<Flights> list = lstflights.getSelectedValuesList();
        if (list.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Select some records first to export them",
                    "Data export", JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        if (jFileChooserCsv.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File chosenFile = jFileChooserCsv.getSelectedFile();
                String chosenFileStr = chosenFile.getCanonicalPath(); // ex
                // Important: append file extension if none was given
                if (!chosenFileStr.matches(".*\\.[^.]{1,10}")) {
                    chosenFileStr += ".csv";
                    chosenFile = new File(chosenFileStr);
                }
                // perform the export
                try (Writer writer = new FileWriter(chosenFile); CSVWriter csvWriter = new CSVWriter(writer)) {
                    String[] headerRecord = {"Id", "nDay","fromCode","toCode","type","passengers"};
                    csvWriter.writeNext(headerRecord);
                    for (Flights flight : list) { // FIXME: what exception does writeNext throw ??? catch it !
                        csvWriter.writeNext(new String[]{flight.toString()});
                    }
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this,
                        "Error writing file:\n" + ex.getMessage(),
                        "File error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }   // TODO add your handling code here:
    }//GEN-LAST:event_miFileSaveActionPerformed

    private void dlgCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgCancelActionPerformed
        
        
        dlgAddFlight.setVisible(false);
        
        
    }//GEN-LAST:event_dlgCancelActionPerformed

    private void miAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miAddActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_miAddActionPerformed

    private void miAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_miAddMouseClicked
        dlgAddFlight.pack();
        dlgAddFlight.setLocationRelativeTo(this);
        dlgAddFlight.setModal(true);
        btSave.setText("Add Flight");
        dlgAddFlight.setVisible(true);
    }//GEN-LAST:event_miAddMouseClicked

    private void btSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSaveActionPerformed
     try{
         ArrayList<String>errorList = new ArrayList<>();
         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       
        Date onDay = dateFormat.parse(dlgDateTf.getText());
        
        String fromCode= dlgFromTf.getText();
        String toCode= dlgToTf.getText();
        String type= String.valueOf(dlgTypeCb.getSelectedItem());
        int passengers=((int)dlgPassengerSld.getValue());
        
       /* 
        if(onDay.isEmpty() | fromCode==null | toCode=null | type==null|passengers=null){
            JOptionPane.showMessageDialog(this,"Entry Error", "All Fields must me completed"
                + "please try again",
                    JOptionPane.ERROR_MESSAGE);
        }
        */
        Flights flight = new Flights(0,onDay,fromCode,toCode,type,passengers);
        
        db.addFlight(flight);
        reloadFromDatabase();
        
        dlgAddFlight.setVisible(false);
     } catch (ParseException ex) {
        JOptionPane.showMessageDialog(this, ex.getMessage(), "Date Format Error"
                + "Must be type as YYYY-MM-DD",
                    JOptionPane.ERROR_MESSAGE);
    } catch (SQLException ex) {
       JOptionPane.showMessageDialog(this, ex.getMessage(), "Database error",
                    JOptionPane.ERROR_MESSAGE);
    
    }//GEN-LAST:event_btSaveActionPerformed
    }
    
    private void dlgPassengerSldStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_dlgPassengerSldStateChanged
       JSlider source = (JSlider)evt.getSource();
          if (!source.getValueIsAdjusting()) {
              int slide = (int)source.getValue();
              dlgPasengerLbl.setText(String.valueOf(slide));
          }

    }//GEN-LAST:event_dlgPassengerSldStateChanged

    private void miFileExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFileExitActionPerformed
    dispose();
    }//GEN-LAST:event_miFileExitActionPerformed

    private void lstflightsMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstflightsMouseReleased
         if (evt.isPopupTrigger()) {
            int index = lstflights.locationToIndex(evt.getPoint());
            lstflights.setSelectedIndex(index);
            popUpDelete.show(this, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_lstflightsMouseReleased

    private void lstflightsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstflightsMouseClicked
        
        if (evt.getClickCount() == 2) { // double-click
            int index = lstflights.locationToIndex(evt.getPoint());
            lstflights.setSelectedIndex(index);
            editCurrSelectedFlights();
        } 
    


    

    }//GEN-LAST:event_lstflightsMouseClicked

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
         try{
           Flights flight =lstflights.getSelectedValue();
           if(flight == null){
               return;
           }
            int input = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete? "+flight.toString() , "DELETING...",
            JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null);
            if(input==1||input==2){
                return;
            }
          db.removeFlight(flight);
            reloadFromDatabase();
            
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to fetch" + ex.getMessage(),
                    "DB ERROR",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jMenuItem1ActionPerformed
 private void editCurrSelectedFlights() {
        Flights flightSelected = lstflights.getSelectedValue();
        if (flightSelected == null) {
            return; // do nothing, no selection
        }
        try {
            // fetch full record, including the blob / image
            Flights currentEditedFlight = db.getFlightsById(flightSelected.id);
            dlgIdLbl.setText(currentEditedFlight.id + "");
            dlgDateTf.setText(String.format("%ty-%tm-%td",currentEditedFlight.onDay,currentEditedFlight.onDay,currentEditedFlight.onDay));
            dlgFromTf.setText(currentEditedFlight.fromCode);
            dlgToTf.setText(currentEditedFlight.toCode);
            //dlgTypeCb.setValue(currentEditedFlight.type);
            dlgPassengerSld.setValue(currentEditedFlight.passengers);
            // byte[] array to BufferedImage to Icon
           
            // show dialog
            dlgAddFlight.pack();
            dlgAddFlight.setLocationRelativeTo(this);
            dlgAddFlight.setModal(true);
            btSave.setText("Update Flight");
            dlgAddFlight.setVisible(true);
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Failed to fetch records: " + ex.getMessage(),
                    "Database error",
                    JOptionPane.ERROR_MESSAGE);
        }
 }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Java3Final.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Java3Final.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Java3Final.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Java3Final.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Java3Final().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btSave;
    private javax.swing.JDialog dlgAddFlight;
    private javax.swing.JButton dlgCancel;
    private javax.swing.JTextField dlgDateTf;
    private javax.swing.JTextField dlgFromTf;
    private javax.swing.JLabel dlgIdLbl;
    private javax.swing.JLabel dlgPasengerLbl;
    private javax.swing.JSlider dlgPassengerSld;
    private javax.swing.JTextField dlgToTf;
    private javax.swing.JComboBox<String> dlgTypeCb;
    private javax.swing.JFileChooser jFileChooserCsv;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JList<String> jList1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JList<Flights> lstflights;
    private javax.swing.JMenu miAdd;
    private javax.swing.JMenu miFile;
    private javax.swing.JMenuItem miFileExit;
    private javax.swing.JMenuItem miFileSave;
    private javax.swing.JPopupMenu popUpDelete;
    // End of variables declaration//GEN-END:variables
}
