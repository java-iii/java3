/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java3final;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Flights {

    public Flights(int id, Date onDay, String fromCode, String toCode, String type, int passengers) {
        this.id = id;
        this.onDay = onDay;
        this.fromCode = fromCode;
        this.toCode = toCode;
        this.type = type;
        this.passengers = passengers;
    }

    int id;
    Date onDay;
    String fromCode;
    String toCode;
    String type;
    int passengers;

    @Override
    public String toString() {
        
        return String.format("#%d: %ty/%tm/%td, From %s To %s , %s with %d passengers", id, onDay, onDay, onDay, fromCode.toUpperCase(), toCode.toUpperCase(), type.toUpperCase(), passengers);
    }

}
