
package javaiii_friendsdb;

import java.sql.*;
import java.util.ArrayList;

public class Database {
        private static final String dbURL = "jdbc:mysql://localhost:3306/day08people";
        private static final String username = "root";
        private static final String password = "root2021";
        private Connection conn;
        public Database() throws SQLException{
        
            conn = DriverManager.getConnection(dbURL, username, password);
        }
        public ArrayList<Person> getAllPeople() throws SQLException{
            ArrayList<Person> list = new ArrayList<>();
             String sql = "SELECT * FROM people";
            PreparedStatement statement = conn.prepareStatement(sql);
            //good practice to use try with ressources
            try(ResultSet result = statement.executeQuery(sql)){
                while(result.next()){
                    int id = result.getInt("id");
                    String name = result.getString("name");
                    int age = result.getInt("age");
                    //System.out.printf("%d: %s is %d Y/o \n", id, name, age);
                    list.add (new Person(id,name,age));
                    
                }
            }
            return list;
        }
        public void addPerson(Person person) throws SQLException{
            String sql= "INSERT INTO people values(NULL,?,?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1,person.name);
        statement.setInt(2,person.age);
        statement.executeUpdate();
        System.out.println("Record Inserted");
        }
        public void updatePerson(Person person) throws SQLException{
            String sql = "Update people SET name=?, age=? WHERE id=?";
            PreparedStatement statement= conn.prepareStatement(sql);
            statement.setString(1, person.name);
            statement.setInt(2, person.age);
            statement.setInt(3, person.id);
            statement.executeUpdate();
            System.out.println("Record updated id=" +person.id);
        }
        public void removePerson(Person person)throws SQLException{
            String sql="DELETE FROM people WHERE id=?";
            PreparedStatement statement= conn.prepareStatement(sql);
            statement.setInt(1,person.id);
            statement.executeUpdate();
            System.out.println("Record updated "+ person.name+" Removed");
            
        }
}
