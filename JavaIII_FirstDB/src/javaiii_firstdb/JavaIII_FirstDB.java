package javaiii_firstdb;
import static java.lang.Math.random;


import java.util.Random;
import java.sql.*;


public class JavaIII_FirstDB {

    public static void main(String[] args) {
        String dbURL = "jdbc:mysql://localhost:3306/day08people";
        String username = "root";
        String password = "Buck9286";
        Random random= new Random();
        Connection conn= null;
        try {

             conn = DriverManager.getConnection(dbURL, username, password);

            
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        try{
        String sql= "INSERT INTO people values(NULL,?,?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1,"bill");
        statement.setString(2,""+ random.nextInt(100));
        System.out.println("Record Inserted");
        }catch (SQLException ex){
            ex.printStackTrace();
        }
        try{
            String sql = "SELECT * FROM people";
            PreparedStatement statement = conn.prepareStatement(sql);
            //good practice to use try with ressources
            try(ResultSet result = statement.executeQuery(sql)){
                while(result.next()){
                    int id = result.getInt("id");
                    String name = result.getString("name");
                    int age = result.getInt("age");
                    System.out.printf("%d: %s is %d Y/o \n", id, name, age);
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        
    }

}
